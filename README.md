# ansible-interview-test

## Objective

Create an Ansible playbook that accomplishes the following:

* Install apache 2.4
* Install mariadb
* Configure apache to serve the index.html in this repository
* Configure mariadb and apache to start when the server is started
* Open ports 80 and 443 in firewalld
* Set selinux to enforcing and persist it

All this should be done by the user ansible (create it beforehand)

When you are done, create a merge request it to this repository.